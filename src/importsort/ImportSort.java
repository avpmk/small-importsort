package importsort;

import importsort.model.SrcModel;
import importsort.util.IterableIterator;
import importsort.view.ConsolePresenter;
import importsort.view.Presenter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;

import static importsort.util.FsUtil.chooseTemporyFileName;
import static importsort.util.IterableIterator.iterableIterator;
import static importsort.util.StringUtil.endsWith_ignoreCase;
import static importsort.util.StringUtil.notEmpty;
import static importsort.util.Sugar.oneOf;
import static importsort.util.ToString.to_s;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.delete;
import static java.nio.file.Files.isDirectory;
import static java.nio.file.Files.isRegularFile;
import static java.nio.file.Files.move;
import static java.nio.file.Files.newBufferedReader;
import static java.nio.file.Files.newBufferedWriter;
import static java.nio.file.Files.newDirectoryStream;
import static ru.tensor.petrov.util.jarpath.JarPath.jarDir;

public class ImportSort {

    private static final String[] DIRS_TO_SKIP = {".git"};

    private static Presenter view = new ConsolePresenter();

    public static void main(String[] args) throws IOException {
        Path fileRoot = jarDir(ImportSort.class);
//        Path fileRoot = Paths.get("/home/Andrei/apps/asyncjdbc-minimized").toRealPath();
        view.folder(fileRoot);

        handleDir(fileRoot);
    }

    public static void handleDir(Path pathDir) throws IOException {
        try (DirectoryStream<Path> directoryStream = newDirectoryStream(pathDir)) {
            for (Path p : directoryStream) {
                if (isRegularFile(p) && endsWith_ignoreCase(to_s(p.getFileName()), ".java")) {
                    handleFile(p);
                } else if (isDirectory(p) && !oneOf(to_s(p.getFileName()), DIRS_TO_SKIP)) { //todo ignore case?
                    handleDir(p);
                }
            }
        }
    }

    public static void handleFile(Path fileSrc) throws IOException {
        boolean generated = false;
        Path fileTmp = null;

        try (BufferedReader reader = newBufferedReader(fileSrc, UTF_8)) {
            SrcModel srcModel = new SrcModel(); //todo reuse
            IterableIterator<String> it = iterableIterator(reader.lines());

            for (String line : it) {
                if (srcModel.tryPut(line)) {
                    reader.mark(100_000);
                } else {
                    reader.reset();
                    break;
                }
            }

            if (srcModel.isNeedSubstitution()) {
                fileTmp = chooseTemporyFileName(fileSrc);
                try (Writer writer = newBufferedWriter(fileTmp, UTF_8)) {
                    //<editor-fold defaultstate="collapsed" desc="writeTo file">
                    srcModel.writeTo(writer);

                    if (it.hasNext()) {
                        String line = it.next();
                        if (notEmpty(line)) {
                            writer.write(line);
                        }
                    }

                    for (String line : it) {
                        writer.write("\n");
                        if (notEmpty(line)) {
                            writer.write(line);
                        }
                    }
                    //</editor-fold>
                    generated = true;
                    view.generated(fileSrc);
                } catch (IOException e) {
                    view.hasErrorOnGeneration(e, fileSrc);
                    try {
                        delete(fileTmp);
                    } catch (IOException onDeleteException) {
                        view.cantDeleteTemporyFile(onDeleteException, fileTmp);
                    }
                }
            } else {
                view.notNeedSubstitution(fileSrc);
            }
        }

        if (generated) { substitute(fileSrc, fileTmp); }

    }

    public static void substitute(Path fileSrc, Path fileTmp) {
        try {
            delete(fileSrc);
        } catch (IOException e) {
            view.cantDeleteSourceFile(e, fileTmp);
            return;
        }

        try {
            move(fileTmp, fileSrc);
            view.substituted(fileSrc);
        } catch (IOException e) {
            view.cantSubstitute(e, fileSrc); //вообще треш!!!! не консистентное состояние, без version control'а делать нечего
        }
    }
}