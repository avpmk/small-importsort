package importsort.view;

import java.io.IOException;
import java.nio.file.Path;

import static java.lang.System.out;

public class ConsolePresenter implements Presenter {

    @Override public void folder(Path fileRoot) {
        out.println("Папка:\n  " + fileRoot);
    }

    @Override public void generated(Path pathFile) {
        out.println("+ " + pathFile.getFileName());
    }

    @Override public void notNeedSubstitution(Path pathFile) {
        out.println("- " + pathFile.getFileName());
    }

    @Override public void hasErrorOnGeneration(IOException e, Path pathFile) {
        out.println(
                "! " + pathFile.getFileName() + ("\n" +
                "  Удаление временного файла")
        );
    }

    @Override public void cantDeleteTemporyFile(IOException onDeleteException, Path temporyFile) {
        out.println(
                ("Программа не может удалить временный файл." + "\n" +
                 "  Причина: ") + onDeleteException + ("\n" +
                 "  Находится здесь: ") + temporyFile + "\n"
        );
    }

    @Override public void cantDeleteSourceFile(IOException onDeleteException, Path tmp) {
        out.println("############################################");
    }

    @Override public void substituted(Path fileSrc) {
        out.println("  Успешно заменён");
    }

    @Override public void cantSubstitute(IOException onMoveException, Path fileDest) {
        out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }
}