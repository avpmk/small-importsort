package importsort.view;

import java.io.IOException;
import java.nio.file.Path;

public interface Presenter {

    void folder(Path fileRoot);

    void notNeedSubstitution(Path pathFile);
    void generated(Path pathFile);

    void substituted(Path fileSrc);
    void cantSubstitute(IOException onMoveException, Path fileDest);

    void hasErrorOnGeneration(IOException e, Path pathFile);
    void cantDeleteTemporyFile(IOException onDeleteException, Path temporyFile);
    void cantDeleteSourceFile(IOException onDeleteException, Path temporyFile);

}