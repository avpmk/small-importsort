package importsort.model;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static importsort.model.CommentedLinesComparator.commentedLinesComparator;
import static importsort.util.IterableUtil.isSorted;
import static importsort.util.StringUtil.empty;
import static importsort.util.StringUtil.notEmpty;

public class SrcModel {

    private StringBuilder head = new StringBuilder(500);

    private final List<String>
            importsUsual  = new ArrayList<>(100),
            importsStatic = new ArrayList<>(100);

    //<editor-fold defaultstate="collapsed" desc="needSubstitution">
    private boolean needSubstitution;

    public boolean hasImports() {
        return !importsUsual.isEmpty() || !importsStatic.isEmpty();
    }

    public boolean isNeedSubstitution() {
        return
                hasImports() &&
                (needSubstitution ||
                !hasDelimeterBetweenUsualAndStaticSection ||
                !isSorted(importsUsual, commentedLinesComparator()) ||
                !isSorted(importsStatic, commentedLinesComparator()) ||
                ((importsUsual.isEmpty() || importsStatic.isEmpty()) && cntEmptyLines_inImportsBlock > 1) ||
                cntEmptyLines_inImportsBlock > 2);
    }
    //</editor-fold>

    private boolean hasStaticImport, hasDelimeterBetweenUsualAndStaticSection;

    private String prevLine = "";

    private boolean storeIfImportExp(String line) {
        boolean imp = line.startsWith("import ") || line.startsWith("//import ");

        if (imp) {
            if (line.startsWith("import static") || line.startsWith("//import static")) {
                importsStatic.add(line);
                if (!hasStaticImport) {
                    hasDelimeterBetweenUsualAndStaticSection = empty(prevLine);
                    hasStaticImport = true;
                }
            } else {
                importsUsual.add(line);
                if (hasStaticImport) {
                    needSubstitution = true;
                }
            }
        }
        prevLine = line;

        return imp;
    }

    //<editor-fold defaultstate="collapsed" desc="strategies">
    private Predicate<String> tryPut = line -> {
        if (storeIfImportExp(line)) {
            strategy2_StoreImports();
        } else if (notEmpty(line)) {
            head.append(line);
            strategy1_HasNotImports();
        } else {
            needSubstitution = true;
        }
        return true;
    };

    private void strategy1_HasNotImports() {
        tryPut = line -> {
            if (storeIfImportExp(line)) {
                strategy2_StoreImports();
            } else {
                head.append("\n");
                head.append(line);
            }
            return true;
        };
    }

    private int cntEmptyLines_inImportsBlock;

    public boolean incCntEmptyLines() {
        cntEmptyLines_inImportsBlock++;
        return true;
    }

    private void strategy2_StoreImports() {
        tryPut = line -> storeIfImportExp(line) || empty(line) && incCntEmptyLines();
    }
    //</editor-fold>

    public boolean tryPut(String line) {
        return tryPut.test(line);
    }

    public void writeTo(Writer writer) throws IOException {
        if (head.length() > 0) {
            writer.write(head.toString());
            writer.write("\n");
        }

        if (!importsUsual.isEmpty()) {
            importsUsual.sort(commentedLinesComparator());
            for (String line : importsUsual) {
                writer.write(line);
                writer.write("\n");
            }
            writer.write("\n");
        }

        if (!importsStatic.isEmpty()) {
            importsStatic.sort(commentedLinesComparator());
            for (String line : importsStatic) {
                writer.write(line);
                writer.write("\n");
            }
            writer.write("\n");
        }
    }
}