package importsort.model;

import java.util.Comparator;

import static importsort.util.StringCompareWithShift.compareShiftOne;

public class CommentedLinesComparator {

    /** Для того чтобы не инстанцировать его каждый раз. (т.к. λ ничего не capture'ит - это singleton) */
    public static Comparator<String> commentedLinesComparator() {
        return (a, b) -> {
            boolean aCommented = a.startsWith("//");
            return aCommented == b.startsWith("//")
                    ? a.compareTo(b)
                    : compareShiftOne(a, b, aCommented, 2);
        };
    }
}