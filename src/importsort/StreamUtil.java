package importsort;

import java.util.Iterator;
import java.util.stream.BaseStream;

public class StreamUtil {

    /** Не отложенный вызов итератора. */
    public static <T, S extends BaseStream<T, S>> Iterable<T> asIterable(BaseStream<T, S> stream) {
        Iterator<T> iterator = stream.iterator();
        return () -> iterator;
    }
}