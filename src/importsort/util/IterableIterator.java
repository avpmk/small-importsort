package importsort.util;

import java.util.Iterator;
import java.util.stream.BaseStream;

public class IterableIterator<T> implements Iterable<T>, Iterator<T> {

    private final Iterator<T> it;

    public IterableIterator(Iterator<T> it) {
        this.it = it;
    }

    public static <T> IterableIterator<T> extractIterator(Iterable<T> iterable) {
        return new IterableIterator(iterable.iterator());
    }

    public static <T, S extends BaseStream<T, S>> IterableIterator<T> iterableIterator(BaseStream<T, S> stream) {
        return new IterableIterator(stream.iterator());
    }

    @Override public Iterator<T> iterator() {
        return this;
    }

    @Override public boolean hasNext() {
        return it.hasNext();
    }

    @Override public T next() {
        return it.next();
    }
}