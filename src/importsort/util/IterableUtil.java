package importsort.util;

import java.util.Comparator;
import java.util.Iterator;

public class IterableUtil {

    public static <T> boolean isSorted(Iterable<T> iterable, Comparator<T> comparator) {
        Iterator<T> it = iterable.iterator();
        if (!it.hasNext()) {
            return true;
        }
        for (T prev = it.next(); it.hasNext();) {
            T curr = it.next();
            if (comparator.compare(prev, curr) > 0) {
                return false;
            }
            prev = curr;
        }
        return true;
    }
}