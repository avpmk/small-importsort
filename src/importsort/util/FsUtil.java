package importsort.util;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static importsort.util.ToString.to_s;
import static java.nio.file.Files.delete;
import static java.nio.file.Files.exists;

public class FsUtil {
    
    /** Подбор имени для временного файла. */
    public static Path chooseTemporyFileName(Path pathFile) {
        String temporyFilePath = to_s(pathFile).concat(".temporyFile");
        Path temporyFile = Paths.get(temporyFilePath);

        for (int i = 0; exists(temporyFile); ++i) {
            try {
                delete(temporyFile);
                return temporyFile;
            } catch (IOException e) {
                temporyFile = Paths.get(temporyFilePath + "_" + i);
            }
        }

        return temporyFile;
    }
}