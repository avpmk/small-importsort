package importsort.util;

public class StringCompareWithShift {

    /**
     * Даёт немного другие результаты если текст подлежащий сравнению является
     * подмножеством другого текста (подлежащего сравнению).
     *
     * @see importsort.util.StringCompareWithShiftTest.compareShift_sequenceIsSubSequenceOfAnther_worksDifferentThanStringsCompareTo()
     */
    public static int compare(String textA, String textB, int shiftA, int shiftB) {
        int limA = textA.length();
        int limB = textB.length();

        while (shiftA < limA && shiftB < limB) {
            char a = textA.charAt(shiftA++);
            char b = textB.charAt(shiftB++);

            if (a != b) {
                return a - b;
            }
        }

        if (shiftA < limA) {
            return textA.charAt(shiftA);
        } else if (shiftB < limB) {
            return textB.charAt(shiftB);
        }

        return 0;
    }

    public static int compareShiftA(String a, String b, int shiftA) {
        return compare(a, b, shiftA, 0);
    }

    public static int compareShiftB(String a, String b, int shiftB) {
        return compare(a, b, 0, shiftB);
    }

    public static int compareShiftOne(String a, String b, boolean shiftAOrB, int shift) {
        return shiftAOrB
                ? compare(a, b, shift, 0)
                : compare(a, b, 0, shift);
    }
}