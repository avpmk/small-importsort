package importsort.util;

import static java.lang.Character.toLowerCase;

public class StringUtil {

    /** @param suffix Если отправить пустой суффикс - вернёт true. */
    public static boolean endsWith_ignoreCase(CharSequence text, CharSequence suffix) {
        int s = suffix.length(), t = text.length();
        if (t < s) {
            return false;
        }
        while (s > 0) {
            if (notEquals_ignoreCase(text.charAt(--t), suffix.charAt(--s))) {
                return false;
            }
        }
        return true;
    }

    private static boolean notEquals_ignoreCase(char a, char b) {
        return a != b && toLowerCase(a) != b && toLowerCase(b) != a;
    }

    /** Нормальных (непрбельных) символов - нет. */
    public static boolean empty(String text) {
        return text.isEmpty() || text.trim().isEmpty();
    }

    /** Есть непрбельные символы. */
    public static boolean notEmpty(String text) {
        return !text.isEmpty() && !text.trim().isEmpty();
    }
}