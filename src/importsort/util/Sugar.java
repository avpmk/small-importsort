package importsort.util;

public class Sugar {
    public static <T> boolean oneOf(T s, T... someObjects) {
        for (T anObject : someObjects) {
            if (s.equals(anObject)) {
                return true;
            }
        }
        return false;
    }
}