package importsort.util;

/** Класс предоставляющий статические методы-обёртки приводящие примитивные типы к строке. */
public class ToString {

    public static String to_s(int value) {
        return Integer.toString(value);
    }

    public static String to_s(long value) {
        return Long.toString(value);
    }

    public static String to_s(double value) {
        return Double.toString(value);
    }

    /**
     * Для {@link SystemOut#println} не имеет смысла. Если передать null выбросит NPE
     * (а не вернёт текст "null" как String.valueOf).
     *
     * @throws NullPointerException
     */
    @Deprecated
    public static String to_s(Object obj) throws NullPointerException {
        return obj.toString();
    }
}