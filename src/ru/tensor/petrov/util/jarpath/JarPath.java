package ru.tensor.petrov.util.jarpath;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JarPath {

    public static Path jarDir(Class cls) {
        try {
            URI uriJarLocation = cls.getProtectionDomain().getCodeSource().getLocation().toURI();
            return Paths.get(uriJarLocation).getParent();
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }
}