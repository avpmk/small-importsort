package importsort.util;

import org.junit.Test;

import static importsort.util.StringCompareWithShift.compare;
import static importsort.util.StringCompareWithShift.compareShiftA;
import static importsort.util.StringCompareWithShift.compareShiftB;
import static org.junit.Assert.*;

public class StringCompareWithShiftTest {

    @Test public void compareShift_sequenceIsSubSequenceOfAnther_worksDifferentThanStringsCompareTo() {
        assertTrue(
                "A".compareTo("Aeq")
                !=
                compare(P1 + "A", P2 + "Aeq", P1.length(), P2.length())
        );
    }

    @Test public void compareShiftA_empty_eq() {
        assertEquals(0, compareShiftA("", "", 0));
    }

    @Test public void compareShiftA_empty_eq_wrongShift() {
        assertEquals(0, compareShiftA("", "", 2));
    }

    private static final String
            P1 = "C:\\SomeFolder\\",
            P2 = "D:\\SomeAotherFolder\\";

    @Test public void compareShift_prefixes_and_emptyText_eq() {
        assertEquals(
                0, compare(P1 + "", P2 + "", P1.length(), P2.length())
        );
    }

    @Test public void compareShift_prefixes_and_eqText_eq() {
        assertEquals(
                0, compare(P1 + "XYZ", P2 + "XYZ", P1.length(), P2.length())
        );
    }

    @Test public void compareShift_prefixes_and_nonEqText_notEq() {
        assertTrue(
                0 != compare(P1 + "XY", P2 + "XYZ", P1.length(), P2.length())
        );
    }

    @Test public void compareShift_prefixes_and_firstLessThanSecondText() {
        assertTrue(
                compare(P1 + "A", P2 + "B", P1.length(), P2.length())
                < 0
        );
    }

    @Test public void compareShift_prefixes_and_textDiffInOneChar() {
        assertEquals(
                "A".compareTo("B"),
                compare(P1 + "A", P2 + "B", P1.length(), P2.length())
        );
    }

    @Test public void compareShiftA_2_ggg_eq() {
        assertEquals(0, compareShiftA("//ggg", "ggg", 2));
    }

    @Test public void compareShiftB_2_ggg_eq() {
        assertEquals(0, compareShiftB("ggg", "//ggg", 2));
    }
}