package importsort.util;

import java.util.List;
import org.junit.Test;

import static importsort.model.CommentedLinesComparator.commentedLinesComparator;
import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class ComparatorPartTextComparingTest {

    @Test public void t1() {
        List<String> list = asList(
                "B", "//D", "D", "C"
        );

        list.sort(commentedLinesComparator());

        assertEquals(
                asList("B", "C", "//D", "D"),
                list
        );
    }
}