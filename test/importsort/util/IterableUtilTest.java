package importsort.util;

import org.junit.Test;

import static importsort.util.IterableUtil.isSorted;
import static java.util.Arrays.asList;
import static java.util.Comparator.naturalOrder;
import static org.junit.Assert.*;

public class IterableUtilTest {

    @Test public void sortedList() {
        assertTrue(
                isSorted(asList(10, 20, 30, 40), naturalOrder())
        );
    }

    @Test public void notSortedList() {
        assertFalse(
                isSorted(asList(10, 20, 50, 40), naturalOrder())
        );
    }
}